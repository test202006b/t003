package com.example.traning.domain.mapper;
 
import com.example.traning.domain.entity.LoginUser;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
 
@Mapper
public interface LoginMapper{
 
    @Select("SELECT userId,userpassword FROM t_user WHERE userId = #{username}")
    LoginUser findUser(String username);
 
}