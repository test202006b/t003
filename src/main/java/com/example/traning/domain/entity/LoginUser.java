package com.example.traning.domain.entity;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class LoginUser{
    
 	@NotNull
	private String userId;
 
	@NotNull
	private String userPassword;

}